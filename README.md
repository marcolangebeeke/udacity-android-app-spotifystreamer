# Udacity Android SpotifyStreamer


###Welcome to the SpotifyStreamer Android App project
With the SpotifyStreamer Android app you can search for artists, find the Top 10 tracks per artists, and even play the first 30 seconds of each track, for FREE! :)

####Spotify
It uses the [Spotify Web API](https://developer.spotify.com/web-api/) to download the data about artists and tracks.


|Search Artist|Artist Top Tracks|Track Player|
|---|---|---|
| ![Search Artist](./doc/screenshots/search-artist-2015-12-09-130537.png?raw=true "Search Artist") | ![Artist Top Tracks](./doc/screenshots/artist-toptracks-2015-12-09-130637.png?raw=true "Artist Top Tracks") |![Track Player](./doc/screenshots/track-player-2015-12-09-130712.png?raw=true "Track Player")|


###Release notes - v1.0

#####Requirements
Android 4.1 Jelly Bean or later (API level 16)


###App diagram
![App diagram](./doc/SpotifyStreamer.P2.2.png?raw=true "App diagram")
