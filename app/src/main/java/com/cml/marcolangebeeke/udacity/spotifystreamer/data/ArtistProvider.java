package com.cml.marcolangebeeke.udacity.spotifystreamer.data;

import android.content.ContentUris;
import android.content.ContentValues;
import android.net.Uri;

import java.util.List;

import de.triplet.simpleprovider.AbstractProvider;
import de.triplet.simpleprovider.Column;
import de.triplet.simpleprovider.Table;

/**
 * ArtistProvider class - ContentProvider for local storage of the Spotify API data
 */
public class ArtistProvider extends AbstractProvider
{
    /**
     * Set the table names
     */
    private static final String tableQueries = "queries";
    private static final String tableArtists = "artists";
    private static final String tableTracks = "tracks";

    /**
     * Construct the uris from the package- and table names
     */
    public static Uri uriQueries = Uri.parse("content://" + ArtistProvider.class.getPackage().getName() + "/"+ tableQueries);
    public static Uri uriArtists = Uri.parse("content://" + ArtistProvider.class.getPackage().getName() + "/"+ tableArtists);
    public static Uri uriTracks = Uri.parse("content://" + ArtistProvider.class.getPackage().getName() + "/"+ tableTracks);

    /**
     * Get the content provider authority name
     *
     * @return String containing the authority name
     */
    @Override
    protected String getAuthority()
    {
        return ArtistProvider.class.getPackage().toString();
    }

    /**
     * Get the version number of the table model definition
     *
     * @return int containg the schema version
     */
    @Override
    protected int getSchemaVersion()
    {
        return 1;
    }

    /**
     * /queries - Search table columns definition
     */
    @Table (tableQueries)
    public class Query
    {
        @Column(value = Column.FieldType.INTEGER, primaryKey = true)
        public static final String COL_ID = "_id";

        @Column(value = Column.FieldType.TEXT, unique = true)
        public static final String COL_QUERY = "query";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_CREATED = "created";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_MODIFIED = "modified";
    }

    /**
     * /artists - Artist table columns definition
     */
    @Table (tableArtists)
    public class Artist
    {
        @Column(value = Column.FieldType.INTEGER, primaryKey = true)
        public static final String COL_ID = "_id";

        @Column(Column.FieldType.TEXT)
        public static final String COL_SPOTIFY_ID = "spotify_id";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_SEARCH_ID = "search_id";

        @Column(Column.FieldType.TEXT)
        public static final String COL_NAME = "name";

        @Column(Column.FieldType.TEXT)
        public static final String COL_IMAGE_URL = "image_url";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_POSITION = "position";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_CREATED = "created";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_MODIFIED = "modified";
    }

    /**
     * /tracks - Track table columns definition
     */
    @Table (tableTracks)
    public class Track
    {
        @Column(value = Column.FieldType.INTEGER, primaryKey = true)
        public static final String COL_ID = "_id";

        @Column(Column.FieldType.TEXT)
        public static final String COL_SPOTIFY_TRACK_ID = "spotify_track_id";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_ARTIST_ID = "artist_id";

        @Column(Column.FieldType.TEXT)
        public static final String COL_SPOTIFY_ARTIST_ID = "spotify_artist_id";

        @Column(Column.FieldType.TEXT)
        public static final String COL_NAME = "name";

        @Column(Column.FieldType.TEXT)
        public static final String COL_ALBUM_NAME = "album_name";

        @Column(Column.FieldType.TEXT)
        public static final String COL_THUMBNAIL_URL = "thumbnail_url";

        @Column(Column.FieldType.TEXT)
        public static final String COL_IMAGE_URL = "image_url";

        @Column(Column.FieldType.TEXT)
        public static final String COL_PREVIEW_URL = "preview_url";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_POSITION = "position";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_CREATED = "created";

        @Column(Column.FieldType.INTEGER)
        public static final String COL_MODIFIED = "modified";
    }

    /**
     * Override of the insert function of the AbstractProvider class, in a way that it uses
     *  the SQLite insertOrThrow function that throws a SQLiteConstraintException when trying to
     *  insert insert a duplicate column value, and we can act on that
     *
     * @param uri Uri
     * @param values ContentValues
     * @return Uri to the inserted row
     */
    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        List<String> segments = uri.getPathSegments();
        if (segments == null || segments.size() != 1) {
            return null;
        }

        long rowId = mDatabase.insertOrThrow(segments.get(0), null, values);

        if (rowId > -1) {
            getContext().getContentResolver().notifyChange(uri, null);

            return ContentUris.withAppendedId(uri, rowId);
        }

        return null;
    }
}