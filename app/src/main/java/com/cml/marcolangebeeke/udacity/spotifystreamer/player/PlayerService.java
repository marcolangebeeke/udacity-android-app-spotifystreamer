package com.cml.marcolangebeeke.udacity.spotifystreamer.player;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.TrackItem;

/**
 * PlayerService class
 */
public class PlayerService extends Service implements
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnErrorListener
{
    // use classname when logging
    private final String LOG_TAG = PlayerService.class.getSimpleName();

    // the playerbinder object of the playerservice
    private final IBinder mPlayerBinder = new PlayerBinder();

    // the mediaplayer object to play the track
    private MediaPlayer mPlayer;

    // the id of the track in the database table tracks
    private int mSelectedTrackId = 0;

    // a list containing the toptracks for selected artist
    private ArrayList<TrackItem> mTopTracks;

    // keep track of the paused state
    private boolean mPaused = false;

    // kep track of the completed state
    private boolean mCompleted = false;

    // messenger caller object to send messages to
    private Messenger mMessenger = null;

    // message types we can receive from the playerservice
    private static final int MSG_TYPE_ON_COMPLETION = 0;
    private static final int MSG_TYPE_ON_PREPARED = 1;
    private static final int MSG_TYPE_CURRENT_POSITION = 2;

    // handler to send update messages about current position
    private Handler mSendPositionHandler = new Handler();
    private static final int DELAY_100MS = 100;
    private static final int DELAY_500MS = 500;
    private static final int DELAY_1000MS = 1000;

    /**
     * A Binder class to bind with the PlayerService
     */
    public class PlayerBinder extends Binder
    {
        public PlayerService getService()
        {
            return PlayerService.this;
        }
    }

    /**
     * On create of the service create the mediaplayer object and define the eventhandlers
     */
    @Override
    public void onCreate()
    {
        super.onCreate();

        // instantiate the mediaplayer
        mPlayer = new MediaPlayer();
        mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        // set eventhandlers to be local
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnSeekCompleteListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);
    }

    /**
     * Select the track to be played
     * @param trackId int
     * @return int
     */
    public int selectTrack(int trackId)
    {
        if (trackId != 0) {
            mSelectedTrackId = trackId;
        }

        return mSelectedTrackId;
    }

    public int getSelectedTrackId()
    {
        return mSelectedTrackId;
    }

    /**
     * Set the list of tracks to play
     * @param topTracks ArrayList
     */
    public void setTopTracks(ArrayList<TrackItem> topTracks)
    {
        mTopTracks = topTracks;
    }

    /**
     * Start the currently selected track
     */
    public void play()
    {
        // reset the player before playing the track
        mPlayer.reset();

        if (mSelectedTrackId != 0) {

            // get the selected track from the list
            TrackItem track = getSelectedTrack(mSelectedTrackId);

            // set the url of the track as datasource for the mediaplayer
            try {
                mPlayer.setDataSource(track.getPreviewUrl());
            } catch (Exception e) {
                Log.e(LOG_TAG, "============> Error setting data source", e);
            }

            // wait for the stream to be ready to start playing
            mPlayer.prepareAsync();
        }
    }

    /**
     * Pause a currently playing track
     */
    public void pause()
    {
        // pause playing
        mPlayer.pause();

        // pause the mediaplayer state
        mPaused = true;
    }

    /**
     * Resume playing a paused track
     */
    public void resume()
    {
        if (mPaused && mSelectedTrackId != 0) {

            // start playing
            mPlayer.start();

            // un-pause the mediaplayer state
            mPaused = false;

            // inform the activity about the current position of the mediaplayer
            sendPositionMessageDelayed();
        }
    }

    /**
     * Move the currentposition of the mediaplayer
     * @param position int
     */
    public void seek(int position)
    {
        mPlayer.seekTo(position);
    }

    /**
     * Stop currently playing track
     */
    public void stop()
    {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        mPaused = false;
    }

    /**
     * Select, and optionally play, the previous track of the toptracks list
     * @return int
     */
    public int playPrevious()
    {
        int previousTrackId = 0;

        // get the previous, or last if current is first, track
        for (int i=0; i < mTopTracks.size(); i++) {
            if (mTopTracks.get(i).getId() == mSelectedTrackId) {
                if (i == 0) {
                    previousTrackId = mTopTracks.get(mTopTracks.size()-1).getId();
                } else {
                    previousTrackId = mTopTracks.get(i-1).getId();
                }
            }
        }

        // update the currently selected track
        selectTrack(previousTrackId);

        // start playing previous track if player was already playing or paused
        if (mPlayer.isPlaying() || mPaused) {
            play();
        }

        return mSelectedTrackId;
    }

    /**
     * Select, and optionally play, the next track of the toptracks list
     * @return int
     */
    public int playNext()
    {
        int nextTrackId = 0;

        // get the next, or first if current is last, track
        for (int i=0; i < mTopTracks.size(); i++) {
            if (mTopTracks.get(i).getId() == mSelectedTrackId) {
                if (i == (mTopTracks.size()-1)) {
                    nextTrackId = mTopTracks.get(0).getId();
                } else {
                    nextTrackId = mTopTracks.get(i+1).getId();
                }
            }
        }

        // update the currently selected track
        selectTrack(nextTrackId);

        // start playing next track if player was already playing or paused
        if (mPlayer.isPlaying() || mPaused) {
            play();
        }

        return mSelectedTrackId;
    }

    /**
     * Get the position in mSec of the currently prepared/playing/paused track
     * @return int
     */
    public int getPosition()
    {
        return mPlayer.getCurrentPosition();
    }

    /**
     * Get the duration in mSec of the currently prepared/playing/paused track
     * @return int
     */
    public int getDuration()
    {
        return mPlayer.getDuration();
    }

    /**
     * Is the mediaplayer currently playing?
     * @return boolean
     */
    public boolean isPlaying()
    {
        return mPlayer.isPlaying();
    }

    /**
     * Is the mediaplyer currently paused?
     * @return booleand
     */
    public boolean isPaused()
    {
        return mPaused;
    }

    /**
     * HAs the mediaplayer just completed playing a track?
     * @return boolean
     */
    public boolean isCompleted()
    {
        return mCompleted;
    }

    /**
     * When the player is prepared and has the stream loaded to start playing
     * @param player MediaPlayer
     */
    @Override
    public void onPrepared(MediaPlayer player)
    {
        Log.i(LOG_TAG, "============> onPrepared called");

        // start playing
        player.start();

        mPaused = false;

        mCompleted = false;

        // inform the activity that the currently selected track is prepared
        Bundle message = new Bundle();
        message.putInt("trackId", mSelectedTrackId);
        message.putInt("trackDuration", getDuration());
        sendMessage(MSG_TYPE_ON_PREPARED, message);

        // inform the activity about the current position of the mediaplayer
        sendPositionMessageDelayed();
    }

    /**
     * Get notified when the mediaplayer has completed the seek action
     * @param player MediaPlayer
     */
    @Override
    public void onSeekComplete(MediaPlayer player)
    {
        Log.i(LOG_TAG, "============> onSeekComplete called: " + getPosition());
    }

    /**
     * When the mediaplayer has finished playing the track
     * @param player MediaPlayer
     */
    @Override
    public void onCompletion(MediaPlayer player)
    {
        Log.i(LOG_TAG, "============> onCompletion called");

        mPaused = false;
        mCompleted = true;

        // inform the activity that the currently selected track has completed playing
        Bundle message = new Bundle();
        message.putInt("trackId", mSelectedTrackId);
        sendMessage(MSG_TYPE_ON_COMPLETION, message);
    }

    /**
     * When the service is bound return the service binder
     * @param intent Intent
     * @return IBinder
     */
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.i(LOG_TAG, "============> onBind called");

        return mPlayerBinder;
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.i(LOG_TAG, "============> onUnbind called");

        return false;
    }

    /**
     * Handle an error with the mediaplayer object
     * @param mp MediaPlayer
     * @param what int
     * @param extra int
     * @return boolean
     */
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        Log.i(LOG_TAG, "============> onError called");

        mPlayer.reset();

        return false;
    }

    /**
     * Get trackitem with given track id from the list of toptracks
     * @param selectedTrackId int
     * @return TrackITem
     */
    private TrackItem getSelectedTrack(int selectedTrackId)
    {
        TrackItem selectedTrack = null;

        // find selected track in the toptracks list
        for (TrackItem track : mTopTracks) {
            if (track.getId() == selectedTrackId) {
                selectedTrack = track;
            }
        }

        return selectedTrack;
    }

    /**
     * Set the messenger object
     * @param messenger Messenger
     */
    public void setMessenger(Messenger messenger)
    {
        mMessenger = messenger;
    }

    /**
     * Remove the messenger object
     */
    public void removeMessenger()
    {
        mMessenger = null;
    }

    /**
     * Send a message of a certain type and with a bundle body back to the activity using the messenger
     * @param messageType int
     * @param bundle Bundle
     */
    private void sendMessage(int messageType, Bundle bundle)
    {
        if (mMessenger != null) {
            try {
                // create a message, set the data bundle, end send it to the messenger
                Message msg = Message.obtain(null, messageType);
                msg.setData(bundle);
                mMessenger.send(msg);

            } catch (RemoteException e) {
                mMessenger = null;
            }
        }
    }

    /**
     * Inform the activity about the current position of the mediaplayer, so we can update the seekbar
     */
    private void sendPositionMessageDelayed()
    {
        mSendPositionHandler.postDelayed(new Runnable() {
            public void run() {

                // handle exception, can occur when device is rotated
                try {
                    // only send position updates when a track is playing
                    if (mPlayer != null && mPlayer.isPlaying()) {

                        // create a bundle with data and send the message
                        Bundle message = new Bundle();
                        message.putInt("trackId", mSelectedTrackId);
                        message.putInt("trackPosition", getPosition());
                        sendMessage(MSG_TYPE_CURRENT_POSITION, message);

                        // send it again in given delay
                        mSendPositionHandler.postDelayed(this, DELAY_1000MS);
                    }
                } catch (IllegalStateException e) {
                    // Log.d(LOG_TAG, "============> MediaPlayer IllegalStateException: "+ e.getMessage());
                }
            }
        }, DELAY_1000MS);
    }

    /**
     * OnDestroy release the mediaplayer (this is called when the service is stopped, not when it is unbound)
     */
    @Override
    public void onDestroy()
    {
        Log.i(LOG_TAG, "============> onDestroy called while having track selected with id: "+ mSelectedTrackId);

        // destroy mediaplayer
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        mPlayer.reset();
        mPlayer.release();
        mPlayer = null;

        super.onDestroy();
    }
}