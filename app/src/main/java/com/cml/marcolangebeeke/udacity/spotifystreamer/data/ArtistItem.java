package com.cml.marcolangebeeke.udacity.spotifystreamer.data;

import android.database.Cursor;

/**
 * Artist item class containing the details of an artist
 */
public class ArtistItem
{
    // artist id
    private int mId;

    // spotify artist id
    private String mSpotifyId;

    // url of the icon image url
    private String mImageUrl;

    // name of the artist
    private String mArtistName;

    /**
     * Constructor for setting individual member vars
     *
     * @param spotifyId String
     * @param imageUrl String
     * @param artistName String
     */
    public ArtistItem(String spotifyId, String imageUrl, String artistName)
    {
        mSpotifyId = spotifyId;
        mImageUrl = imageUrl;
        mArtistName = artistName;
    }

    /**
     * Constructor using a Cursor object
     *
     * @param cursor Cursor
     */
    public ArtistItem(Cursor cursor)
    {
        setId(cursor.getInt(cursor.getColumnIndex(ArtistProvider.Artist.COL_ID)));
        setSpotifyId(cursor.getString(cursor.getColumnIndex(ArtistProvider.Artist.COL_SPOTIFY_ID)));
        setArtistName(cursor.getString(cursor.getColumnIndex(ArtistProvider.Artist.COL_NAME)));
        setImageUrl(cursor.getString(cursor.getColumnIndex(ArtistProvider.Artist.COL_IMAGE_URL)));
    }

    /**
     * Get the id
     * @return int
     */
    public int getId()
    {
        return mId;
    }

    /**
     * Set the id
     * @param id int
     */
    public void setId(int id)
    {
        mId = id;
    }

    /**
     * Get the spotify id
     * @return String
     */
    public String getSpotifyId()
    {
        return mSpotifyId;
    }

    /**
     * Set the id
     * @param spotifyId String
     */
    public void setSpotifyId(String spotifyId)
    {
        mSpotifyId = spotifyId;
    }

    /**
     * Get the image url
     * @return String
     */
    public String getImageUrl()
    {
        return mImageUrl;
    }

    /**
     * Set the image url
     * @param imageUrl String
     */
    public void setImageUrl(String imageUrl)
    {
        mImageUrl = imageUrl;
    }

    /**
     * Get the name of the artist
     * @return String
     */
    public String getArtistName()
    {
        return mArtistName;
    }

    /**
     * Set the name of the artist
     * @param artistName String
     */
    public void setArtistName(String artistName)
    {
        this.mArtistName = artistName;
    }
}