package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;

/**
 * Custom array adapter for binding a arraylist of trackitem objects to a listview
 */
public class TrackListAdapter extends CursorAdapter
{
    /**
     * Constructor
     *
     * @param context Context
     * @param cursor Cursor
     * @param flags int
     */
    public TrackListAdapter(Context context, Cursor cursor, int flags)
    {
        super(context, cursor, flags);
    }

    /**
     * Instantiate the view
     *
     * @param context Context where the view exists
     * @param cursor Cursor containing data
     * @param parent ViewGroup where the view is part of
     * @return View that was created
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        // create a new view from the list_item_artist definition
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_track, parent, false);

        // set a viewholder with our view layout for the view we just created
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    /**
     * Bind the cursor data to the view
     *
     * @param view View to bind
     * @param context Context where the view exists
     * @param cursor Cursor containing the data
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        // get the viewholder layout containing the view items
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        // get the track name
        String name = cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_NAME));
        viewHolder.titleText.setText(name);

        // get the album name
        String albumName = cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_ALBUM_NAME));
        viewHolder.albumText.setText(albumName);

        // get the album image url
        String imageUrl = cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_IMAGE_URL));
        if ((imageUrl != null) && (imageUrl.length() != 0)) {
            // use picasso to load the image
            Picasso.with(context)
                    .load(imageUrl)
                    .into(viewHolder.iconImage);
        } else {

            // else use the image not available icon
            viewHolder.iconImage.setImageResource(R.mipmap.ic_launcher);
        }
    }

    /**
     * Cache holder containing the child views for a track list item
     */
    public static class ViewHolder
    {
        public final ImageView iconImage;
        public final TextView titleText;
        public final TextView albumText;

        public ViewHolder(View view)
        {
            iconImage = (ImageView) view.findViewById(R.id.track_icon);
            titleText = (TextView) view.findViewById(R.id.track_title);
            albumText = (TextView) view.findViewById(R.id.album_title);
        }
    }
}

