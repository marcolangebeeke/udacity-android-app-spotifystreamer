package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.QueryItem;

/**
 * Main fragment class
 */
public class MainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    // use classname when logging
    private final String LOG_TAG = MainFragment.class.getSimpleName();

    // unique reference for the artist cursor loader
    private static final int ARTIST_LOADER = 0;

    // timeout for reloading the artists from the spotify api
    private static final int CACHE_TIMEOUT = 3600;

    // a common toast object for giving feedback to the user
    protected Toast mToast;

    // the entered search query
    private String mQuery = "";

    // artist list adapter for displaying the result of an artist search in the spotify api
    private ArtistListAdapter mArtistListAdapter;

    /**
     * Callback interface
     */
    public interface Callback
    {
        // callback for when an artist has been clicked and we need to load ArtistFragment
        public void onArtistSelected(int artistId, String artistName);

        // callback for when a new search was done and we need to clear the ArtistFragment
        public void onArtistUnSelected();

        public boolean isTwoPaneDevice();
    }

    /**
     * On create of the main fragment set the fragment_main layout as contentview
     *
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // inflate the fragment_main layout
        final View mainView = inflater.inflate(R.layout.fragment_main, container, false);

        // submit on enter
        final EditText artistName = (EditText) mainView.findViewById(R.id.artist_name);
        artistName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getAction() == KeyEvent.ACTION_DOWN) && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) ||
                        (actionId == EditorInfo.IME_ACTION_DONE)) {

                    // check if an artist name was entered
                    if (artistName.getText().toString().trim().length() != 0) {

                        // safe the query to the fragment member var
                        mQuery = artistName.getText().toString();

                        // find the artists in the database, or call the spotify api using an async task when the cache timeout is gone
                        getArtists(mQuery);

                        // clear previously selected artist if we are one a 'twopane' device
                        if (((Callback) getActivity()).isTwoPaneDevice()) {
                            ((Callback) getActivity()).onArtistUnSelected();
                        }


                    } else {

                        // clear the fragment member var
                        mQuery = "";

                        // clear the artist adapter, and listview
                        mArtistListAdapter.swapCursor(null);

                        // show toast when no text was entered
                        mToast = Utility.showToast(getActivity(), mToast, getString(R.string.text_input_required) + " artist");
                    }

                    return true;
                }

                return false;
            }
        });

        // create a list adapter and set it to the listview
        mArtistListAdapter = new ArtistListAdapter(getActivity(), null, 0);
        ListView artistListView = (ListView) mainView.findViewById(R.id.listview_artist);
        artistListView.setAdapter(mArtistListAdapter);

        // on click onn a artist list item start the artist activity view and pass the artist name as intent
        artistListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // check network available
                if (Utility.isNetworkAvailable(getActivity())) {

                    // get cursor from the selected item of the cursoradapter connected to the listview
                    Cursor artistCursor = (Cursor) parent.getItemAtPosition(position);
                    if ((artistCursor != null) && (artistCursor.getCount() >= position)) {

                        // create intent to lauch the artist activity
                        Intent artistIntent = new Intent(getActivity(), ArtistActivity.class);

                        // send artist id with intent
                        int artistId = artistCursor.getInt(artistCursor.getColumnIndex(ArtistProvider.Artist.COL_ID));
                        artistIntent.putExtra(ArtistProvider.Artist.COL_ID, artistId);

                        // also send the artist name with the intent
                        String artistName = artistCursor.getString(artistCursor.getColumnIndex(ArtistProvider.Artist.COL_NAME));
                        artistIntent.putExtra(ArtistProvider.Artist.COL_NAME, artistName);

                        // start the activity/fragment using a callback that is handles in the mainactivity
                        ((Callback) getActivity()).onArtistSelected(artistId, artistName);
                    }
                } else {
                    mToast = Utility.showToast(getActivity(), mToast, getString(R.string.network_required_notice));
                }
            }
        });

        // restore a previous state if present
        if (savedInstanceState != null) {

            // check if query was stored
            if (savedInstanceState.containsKey("query")) {
                mQuery = savedInstanceState.getString("query", "");

                // set edittext value if not zero
                if (mQuery.length() != 0) {
                    artistName.setText(mQuery);

                    // find the artists in the database, or call the spotify api using an async task when the cache timeout is gone
                    getArtists(mQuery);
                }
            }
        }

        return mainView;
    }

    /**
     * Save the the current state before leaving the activity
     *
     * @param outState Bundle
     */
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // save the previously searched query string
        outState.putString("query", mQuery);
    }

    /**
     * Initialize the loader once the activity is created
     *
     * @param savedInstanceState Bundle
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        getLoaderManager().initLoader(ARTIST_LOADER, null, this);

        Log.d(LOG_TAG, "============> initLoader to reload artists for query: '" + mQuery + "'");

        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Call the async search artist task
     *
     * @param query String
     */
    private void getArtists(String query)
    {
        // get the current timestamp
        int timestamp = (int) (System.currentTimeMillis()/1000);
        int queryModified = 0;

        // load query details from the database and get the modified field
        QueryItem queryItem = getQueryDetails(query);
        if (queryItem != null) {
            queryModified = queryItem.getModified();
        }

        // check if cache timeout has passed, if so send query to spotify api again
        if ((queryModified == 0) || (timestamp > (queryModified + CACHE_TIMEOUT))) {

            // check network available
            if (Utility.isNetworkAvailable(getActivity())) {

                Log.d(LOG_TAG, "============> Calling spotify API to search artists for query: '" + mQuery + "'");

                // create and execute a new search artist task
                SearchArtistTask searchArtistTask = new SearchArtistTask(this, ARTIST_LOADER, 20);
                searchArtistTask.execute(query);

            } else {
                mToast = Utility.showToast(getActivity(), mToast, getString(R.string.network_required_notice));
            }
        } else {

            Log.d(LOG_TAG, "============> Restarting Loader to reload artists for query: '"+ query +"'");

            // reload the data from the database
            getLoaderManager().restartLoader(ARTIST_LOADER, null, this);
        }
    }

    /**
     * Get the details from the database for given query string and return a queryitem object
     *
     * @param query String
     * @return QueryItem
     */
    public QueryItem getQueryDetails(String query)
    {
        QueryItem queryItem = null;

        // get the query details from the database
        Cursor queryCursor = getActivity().getContentResolver().query(
                ArtistProvider.uriQueries,
                null,
                ArtistProvider.Query.COL_QUERY +" = ?",
                new String[] {query},
                null
        );

        if (queryCursor != null) {
            if (queryCursor.getCount() > 0) {
                queryCursor.moveToFirst();

                // create queryitem object from cursor
                queryItem = new QueryItem(queryCursor);
            }
            queryCursor.close();
        }

        return queryItem;
    }

    /**
     * Load the artists from the database for given query when the loader is called
     *
     * @param i int
     * @param bundle Bundle
     * @return Cursor containing the artists
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle)
    {
        int queryId = 0;

        // load query details from the database and get the modified field
        QueryItem queryItem = getQueryDetails(mQuery);
        if (queryItem != null) {
            queryId = queryItem.getId();
        }

        Log.d(LOG_TAG, "============> Loading artists from database with queryId: " + queryId);

        return new CursorLoader(getActivity(),
            ArtistProvider.uriArtists,
            null,
            ArtistProvider.Artist.COL_SEARCH_ID + " = ?",
            new String[] {String.valueOf(queryId)},
            ArtistProvider.Artist.COL_POSITION +" ASC");
    }

    /**
     * When finished loading from the database, set the loaded cursor to the cursoradapter to update the listview
     *
     * @param cursorLoader Loader
     * @param cursor Cursor
     */
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor)
    {
        mArtistListAdapter.swapCursor(cursor);
    }

    /**
     * Empty the cursoradapter by setting null instead of a cursor
     *
     * @param cursorLoader Loader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader)
    {
        mArtistListAdapter.swapCursor(null);
    }
}

