package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Image;
import kaaes.spotify.webapi.android.models.Track;
import kaaes.spotify.webapi.android.models.Tracks;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistItem;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.TrackItem;

/**
 * ArtistTopTracksTask class for async calling the spotify api for an artists' top tracks
 */
public class GetTracksTask extends AsyncTask<ArtistItem, Void, ArrayList<TrackItem>>
{
    // use classname when logging
    private final String LOG_TAG = GetTracksTask.class.getSimpleName();

    // artistitem object containing the artist details
    private ArtistItem mArtist;

    // the activity context that the adapter is used in
    private ArtistFragment mFragment;

    // the tracks loader id constant from artistfragment
    private int TRACKS_LOADER;

    // country code to get the top 10 tracks
    private String mCountry;

    /**
     * Constructor
     *
     * @param fragment Fragment
     */
    public GetTracksTask(ArtistFragment fragment, int tracksLoaderId)
    {
        mFragment = fragment;
        TRACKS_LOADER = tracksLoaderId;

        mCountry = "US";
    }

    /**
     * Call Spotify artist api in the background
     *
     * @param artists ArtistItem[]
     * @return String
     */
    @Override
    protected ArrayList<TrackItem> doInBackground(ArtistItem[] artists)
    {
        // get the given artistitem
        mArtist = artists[0];

        // create an arraylist for the track list adapter
        ArrayList<TrackItem> trackItems = new ArrayList<TrackItem>();

        // create a hashmap to pass in extra options
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("country", mCountry);

        // create tracks object to store the tracks
        Tracks results = new Tracks();

        // call the spotify service getartisttoptrack api method
        try {
            SpotifyApi api = new SpotifyApi();
            SpotifyService spotify = api.getService();
            results = spotify.getArtistTopTrack(mArtist.getSpotifyId(), options);
        } catch (Exception e) {

            // log the error
            Log.e(LOG_TAG, "============> Spotify.getArtistTopTrack(): ===============> " + e.getMessage());

            return trackItems;
        }

        // get track list from api result
        List tracks = results.tracks;

        Log.e(LOG_TAG, "============> Spotify API returned  "+ tracks.size() +" tracks");

        // get track details and add trackitems to the arraylist
        for (int i=0; i<tracks.size(); i++) {
            String largestImage = "";
            String smallestImage = "";
            String thumbnailUrl = "";
            String imageUrl = "";

            // get track
            Track track = (Track) tracks.get(i);

            // get the album image
            List<Image> images = track.album.images;
            for (int j=0; j<images.size(); j++) {
                Image img = (Image) images.get(j);

                if (j == 0) {
                    // first image is the largest image
                    largestImage = img.url;
                } else if (j == images.size() - 1) {
                    // last image is the smalles image
                    smallestImage = img.url;
                }

                if (img.width == 200) {
                    // find a 200px wide image for the list items
                    thumbnailUrl = img.url;
                } else if (img.width == 640) {
                    // find a 640px wide image for the now playing screen
                    imageUrl = img.url;
                }
            }

            // if list item size not found use the smallest for the thumbnail
            if (thumbnailUrl.length() == 0) {
                thumbnailUrl = smallestImage;
            }

            // if now playing size not found use the largest image
            if (imageUrl.length() == 0) {
                imageUrl = largestImage;
            }

            // create trackitem
            TrackItem trackItem = new TrackItem(track.id, mArtist.getId(), mArtist.getSpotifyId(), track.name, track.album.name, thumbnailUrl, imageUrl, track.preview_url);

            // add trackitem to the arraylist
            trackItems.add(trackItem);
        }

        return trackItems;
    }

    /**
     * On post execute update the contents of the artist list adapter, or show a toast when nothing found
     *
     * @param trackItems ArrayList
     */
    @Override
    protected void onPostExecute(ArrayList<TrackItem> trackItems)
    {
        // check if tracks were found
        if ((trackItems != null) && (trackItems.size() > 0)) {

            // save the loaded tracks to the database
            storeTracks(trackItems);

            // tell the cursor loader to reload
            mFragment.getLoaderManager().restartLoader(TRACKS_LOADER, null, mFragment);

        } else {

            // show a notice using the toast if nothing found
            mFragment.mToast = Utility.showToast(mFragment.getActivity(), mFragment.mToast, mFragment.getString(R.string.tracks_not_found_notice) +": "+ mArtist.getArtistName());
        }
    }

    /**
     * Save the resulting loaded tracks for given artist to the database
     *
     * @param trackItems ArrayList
     */
    private int storeTracks(ArrayList<TrackItem> trackItems)
    {
        int inserted = 0;
        int timestamp = (int) (System.currentTimeMillis()/1000);

        // delete existing tracks of given artist
        int deleted = mFragment.getActivity().getContentResolver().delete(
                ArtistProvider.uriTracks,
                ArtistProvider.Track.COL_ARTIST_ID + " = ?",
                new String[] {String.valueOf(mArtist.getId())}
        );

        Log.d(LOG_TAG, "============> Deleted "+ deleted +" tracks for artist '"+ mArtist.getArtistName() +"' from database");

        // loop through artistItems and insert them all into the db
        ContentValues[] ContentValuesArray = new ContentValues[trackItems.size()];
        for (int i=0; i<trackItems.size(); i++) {
            TrackItem track = trackItems.get(i);

            // created contentvalues pairs to hand over to the bulkinsert method of the contentprovider
            ContentValues trackValues = new ContentValues();
            trackValues.put(ArtistProvider.Track.COL_SPOTIFY_TRACK_ID, track.getSpotifyTrackId());
            trackValues.put(ArtistProvider.Track.COL_ARTIST_ID, mArtist.getId());
            trackValues.put(ArtistProvider.Track.COL_SPOTIFY_ARTIST_ID, mArtist.getSpotifyId());
            trackValues.put(ArtistProvider.Track.COL_NAME, track.getName());
            trackValues.put(ArtistProvider.Track.COL_ALBUM_NAME, track.getAlbumName());
            trackValues.put(ArtistProvider.Track.COL_THUMBNAIL_URL, track.getAlbumThumbnailUrl());
            trackValues.put(ArtistProvider.Track.COL_IMAGE_URL, track.getAlbumImageUrl());
            trackValues.put(ArtistProvider.Track.COL_PREVIEW_URL, track.getPreviewUrl());
            trackValues.put(ArtistProvider.Track.COL_POSITION, i+1);
            trackValues.put(ArtistProvider.Track.COL_CREATED, timestamp);
            trackValues.put(ArtistProvider.Track.COL_MODIFIED, timestamp);
            ContentValuesArray[i] = trackValues;
        }

        // insert artists to the database
        inserted = mFragment.getActivity().getContentResolver().bulkInsert(ArtistProvider.uriTracks, ContentValuesArray);

        Log.d(LOG_TAG, "============> Inserted "+ inserted +" tracks for artist '"+ mArtist.getArtistName() +"' to database");

        return inserted;
    }

}