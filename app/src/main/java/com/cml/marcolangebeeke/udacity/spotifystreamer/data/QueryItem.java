package com.cml.marcolangebeeke.udacity.spotifystreamer.data;

import android.database.Cursor;

/**
 * Query item class containing the details of a query
 */
public class QueryItem
{
    // database query id
    private int mId;

    // query string
    private String mQuery;

    // timestamp created in the database
    private int mCreated;

    // timestamp last modified in the database
    private int mModified;

    /**
     * Constructor for setting individual member vars
     *
     * @param id int
     * @param query String
     * @param created int
     * @param modified int
     */
    public QueryItem(int id, String query, int created, int modified)
    {
        mId = id;
        mQuery = query;
        mCreated = created;
        mModified = modified;
    }

    /**
     * Constructor using a Cursor object
     *
     * @param cursor Cursor
     */
    public QueryItem(Cursor cursor)
    {
        setId(cursor.getInt(cursor.getColumnIndex(ArtistProvider.Query.COL_ID)));
        setQuery(cursor.getString(cursor.getColumnIndex(ArtistProvider.Query.COL_QUERY)));
        setCreated(cursor.getInt(cursor.getColumnIndex(ArtistProvider.Query.COL_CREATED)));
        setModified(cursor.getInt(cursor.getColumnIndex(ArtistProvider.Query.COL_MODIFIED)));
    }

    /**
     * Get the id
     * @return int
     */
    public int getId()
    {
        return mId;
    }

    /**
     * Set the id
     * @param id int
     */
    public void setId(int id)
    {
        this.mId = id;
    }

    /**
     * Get the query string
     * @return String
     */
    public String getQuery()
    {
        return mQuery;
    }

    /**
     * Set the query string
     * @param query String
     */
    public void setQuery(String query)
    {
        this.mQuery = query;
    }

    /**
     * Get the created timestamp
     * @return int
     */
    public int getCreated()
    {
        return mCreated;
    }

    /**
     * Set the created timestamp
     * @param created int
     */
    public void setCreated(int created)
    {
        this.mCreated = created;
    }

    /**
     * Get the last modified timestamp
     * @return int
     */
    public int getModified()
    {
        return mModified;
    }

    /**
     * Set the last modified timestamp
     * @param modified int
     */
    public void setModified(int modified)
    {
        this.mModified = modified;
    }
}
