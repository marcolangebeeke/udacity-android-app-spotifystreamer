package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.ArtistsPager;
import kaaes.spotify.webapi.android.models.Image;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.QueryItem;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistItem;

/**
 * SearchArtistTask class for async querying the spotify api for an artist
 */
public class SearchArtistTask extends AsyncTask<String, Void, ArrayList<ArtistItem>>
{
    // use classname when logging
    private final String LOG_TAG = SearchArtistTask.class.getSimpleName();

    // string for given query
    private String mQuery = "";

    // the activity context that the adapter is used in
    private final MainFragment mFragment;

    // the artistloader constant from mainfragment
    private final int mArtistLoader;

    // amount of artists to fetch from spotify api
    private int mArtistLimit;

    /**
     * Constructor
     *
     * @param fragment Fragment
     */
    public SearchArtistTask(MainFragment fragment, int artistLoader, int limit)
    {
        mFragment = fragment;
        mArtistLoader = artistLoader;
        mArtistLimit = limit;
    }

    /**
     * Call Spotify artist search api in the background
     *
     * @param queryStrings String[]
     * @return String
     */
    @Override
    protected ArrayList<ArtistItem> doInBackground(String[] queryStrings)
    {
        // get the given search query
        mQuery = queryStrings[0];

        // create an arraylist for the artist list adapter
        ArrayList<ArtistItem> artistItems = new ArrayList<ArtistItem>();

        // create a hashmap to pass in extra options
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("limit", mArtistLimit);

        // create artistspager to store the artists
        ArtistsPager results = new ArtistsPager();

        // call the spotify service searchartists api method
        try {
            SpotifyApi api = new SpotifyApi();
            SpotifyService spotify = api.getService();
            results = spotify.searchArtists(mQuery, options);
        } catch (Exception e) {

            // log the error
            Log.e(LOG_TAG, "============> spotify.searchArtists(): " + e.getMessage());

            return artistItems;
        }

        // get artist list from search result
        List artists = results.artists.items;

        // get artist details and add artistitems to the arraylist
        for (int i=0; i<artists.size(); i++) {

            String imageUrl = "";

            // get artist
            Artist artist = (Artist) artists.get(i);

            // get the artist image
            List<Image> images = artist.images;
            for (int j=0; j<images.size(); j++) {
                Image img = (Image) images.get(j);
                imageUrl = img.url;
            }

            // create artistitem
            ArtistItem artistItem = new ArtistItem(artist.id, imageUrl, artist.name);

            // add artistitem to the arraylist
            artistItems.add(artistItem);
        }

        return artistItems;
    }

    /**
     * On post execute update the contents of the artist list adapter, or show a toast when nothing found
     *
     * @param artistItems ArrayList
     */
    @Override
    protected void onPostExecute(ArrayList<ArtistItem> artistItems)
    {
        // check if artists were found
        if ((artistItems != null) && (artistItems.size() > 0)) {

            // store the query in the database
            int queryId = storeQuery(mQuery);

            // store the artists in database
            storeArtists(queryId, artistItems);

            // tell the cursor loader to reload
            mFragment.getLoaderManager().restartLoader(mArtistLoader, null, mFragment);

        } else {

            // show a notice using the toast if nothing found
            mFragment.mToast = Utility.showToast(mFragment.getActivity(), mFragment.mToast, mFragment.getString(R.string.artist_not_found_notice) + ": " + mQuery);
        }
    }

    /**
     * storeQuery - Save the search query to the database
     *
     * @param query String
     */
    private int storeQuery(String query)
    {
        int queryId = 0;
        int timestamp = (int) (System.currentTimeMillis()/1000);

        try {

            // create the values to store the new query
            ContentValues newQueryValues = new ContentValues();
            newQueryValues.put(ArtistProvider.Query.COL_QUERY, query);
            newQueryValues.put(ArtistProvider.Query.COL_CREATED, timestamp);
            newQueryValues.put(ArtistProvider.Query.COL_MODIFIED, timestamp);

            // try to insert a new query record, or catch an exception if it already exists
            Uri inserted = mFragment.getActivity().getContentResolver().insert(
                    ArtistProvider.uriQueries,
                    newQueryValues
            );

            Log.d(LOG_TAG, "============> Inserted query '" + query + "', get it here: " + inserted.toString());

            // get the id from the uri from the inserted row
            queryId = (int) ContentUris.parseId(inserted);

        } catch (SQLiteConstraintException e) {

            // create the values to update the existing query (only update the modified field with a new timestamp)
            ContentValues updatedQueryValues = new ContentValues();
            updatedQueryValues.put(ArtistProvider.Query.COL_MODIFIED, timestamp);

            // update the existing query record
            int updated = mFragment.getActivity().getContentResolver().update(
                    ArtistProvider.uriQueries,
                    updatedQueryValues,
                    ArtistProvider.Query.COL_QUERY +" = ?",
                    new String[] {query}
            );

            Log.d(LOG_TAG, "============> Updated "+ updated +" record for query '"+ query +"'");

            // get the id of the updated query
            QueryItem queryItem = mFragment.getQueryDetails(query);
            if (queryItem != null) {
                queryId = queryItem.getId();
            }
        }

        return queryId;
    }

    /**
     * storeArtist - Save the resulting artists for given query to the database
     *
     * @param artistItems ArrayList
     */
    private int storeArtists(int queryId, ArrayList<ArtistItem> artistItems)
    {
        int inserted = 0;
        int timestamp = (int) (System.currentTimeMillis()/1000);

        if (queryId != 0) {

            // delete existing artists related to given query
            int deleted = mFragment.getActivity().getContentResolver().delete(
                    ArtistProvider.uriArtists,
                    ArtistProvider.Artist.COL_SEARCH_ID + " = ?",
                    new String[] {String.valueOf(queryId)}
            );

            Log.d(LOG_TAG, "============> Deleted " + deleted + " artists for query id " + String.valueOf(queryId));

            // loop through artistItems and insert them all into the db
            ContentValues[] ContentValuesArray = new ContentValues[artistItems.size()];
            for (int i=0; i<artistItems.size(); i++) {
                ArtistItem artist = artistItems.get(i);

                // created contentvalues pairs to hand over to the bulkinsert method of the contentprovider
                ContentValues artistValues = new ContentValues();
                artistValues.put(ArtistProvider.Artist.COL_SPOTIFY_ID, artist.getSpotifyId());
                artistValues.put(ArtistProvider.Artist.COL_SEARCH_ID, queryId);
                artistValues.put(ArtistProvider.Artist.COL_NAME, artist.getArtistName());
                artistValues.put(ArtistProvider.Artist.COL_IMAGE_URL, artist.getImageUrl());
                artistValues.put(ArtistProvider.Artist.COL_POSITION, i+1);
                artistValues.put(ArtistProvider.Artist.COL_CREATED, timestamp);
                artistValues.put(ArtistProvider.Artist.COL_MODIFIED, timestamp);
                ContentValuesArray[i] = artistValues;
            }

            // insert artists to the database
            inserted = mFragment.getActivity().getContentResolver().bulkInsert(ArtistProvider.uriArtists, ContentValuesArray);
        }

        Log.d(LOG_TAG, "============> Inserted "+ inserted +" artists for query '"+ mQuery +"'");

        return inserted;
    }
}
