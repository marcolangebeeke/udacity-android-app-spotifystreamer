package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.TrackItem;
import com.cml.marcolangebeeke.udacity.spotifystreamer.player.PlayerService;
import com.cml.marcolangebeeke.udacity.spotifystreamer.player.PlayerService.PlayerBinder;

/**
 * Player fragment class
 */
public class PlayerFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    // use classname when logging
    private final String LOG_TAG = PlayerFragment.class.getSimpleName();

    private static final String PLAYERDIALOGFRAGMENT_TAG = "PDFTAG";

    // unique reference for the track cursor loader
    private static final int TRACK_LOADER = 2;

    // the id of the track in the database table tracks
    private int mSelectedTrackId = 0;

    // the name of the artists, for convinience we receive in the intent so we dont have to load it from the db
    private String mArtistName;

    // a list containing the toptracks for selected artist
    private ArrayList<TrackItem> mTopTracks;

    // the seekbar to show state of the player, and to control the player
    private SeekBar mPlayerSeekbar;
    private TextView mTrackPosition;
    private TextView mTrackDuration;

    // a common toast object for giving feedback to the user
    protected Toast mToast;

    // the playerservice containing the mediaplayer
    private PlayerService mPlayerService;

    // the intent to pass to the playerservice
    private Intent mPlayIntent;

    // indicator to know if the playerservice is bound
    private boolean mPlayerServiceBound = false;

    // a handler to receive incoming messages from our playerservice
    private IncomingHandler mMessageHandler;

    // message types we can receive from the playerservice
    private static final int MSG_TYPE_ON_COMPLETION = 0;
    private static final int MSG_TYPE_ON_PREPARED = 1;
    private static final int MSG_TYPE_CURRENT_POSITION = 2;

    /**
     * When creating the player view inflate the fragment player view, attach eventhandlers and get
     *  data from the savestate or intent
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.i(LOG_TAG, "============> onCreateView called");

        // get the player fragment layout as defined in the fragment_player xml
        final View playerView = inflater.inflate(R.layout.fragment_player, container, false);

        // seekbar
        mPlayerSeekbar = (SeekBar) playerView.findViewById(R.id.player_seekbar);
        mPlayerSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    updateTimeTextView(mTrackPosition, progress);
                    if (mPlayerServiceBound) {
                        mPlayerService.seek(progress * 1000);
                    }
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        // get a reference to the track position and duration textviews
        mTrackPosition = (TextView) playerView.findViewById(R.id.track_position_textview);
        mTrackDuration = (TextView) playerView.findViewById(R.id.track_duration_textview);

        // play
        ImageView playButton = (ImageView) playerView.findViewById(R.id.player_play_imageview);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressPlay();
            }
        });

        // previous
        ImageView previousButton = (ImageView) playerView.findViewById(R.id.player_previous_imageview);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressPrevious();
            }
        });

        // next
        ImageView nextButton = (ImageView) playerView.findViewById(R.id.player_next_imageview);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressNext();
            }
        });

        // get given arguments (which we retrieve in tablet mode)
        Bundle args = getArguments();

        // restore a previous state if present
        if (savedInstanceState != null) {

            // check if track id was stored
            if (savedInstanceState.containsKey(ArtistProvider.Track.COL_ID)) {
                mSelectedTrackId = savedInstanceState.getInt(ArtistProvider.Track.COL_ID, 0);
            }

            // check if artist name was stored
            if (savedInstanceState.containsKey(ArtistProvider.Artist.COL_NAME)) {
                mArtistName = savedInstanceState.getString(ArtistProvider.Artist.COL_NAME);
            }

        } else if (args != null) {

            // get the track id from the fragment arguments
            mSelectedTrackId = args.getInt(ArtistProvider.Track.COL_ID, 0);

            // get the artist name from the fragment arguments
            mArtistName = args.getString(ArtistProvider.Artist.COL_NAME);

        } else {

            // if no previous state available get the intent given when this activity was launched
            Intent intent = getActivity().getIntent();

            // get the track id given with the intent
            if ((intent != null) && (intent.hasExtra(ArtistProvider.Track.COL_ID))) {
                mSelectedTrackId = intent.getIntExtra(ArtistProvider.Track.COL_ID, 0);
            }

            // get the artist name given with the intent
            if ((intent != null) && (intent.hasExtra(ArtistProvider.Artist.COL_NAME))) {
                mArtistName = intent.getStringExtra(ArtistProvider.Artist.COL_NAME);
            }
        }

        return playerView;
    }

    /**
     * Set certain features if this fragment is shown as a dialog
     * @param savedInstanceState Bundle
     * @return Dialog
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // hide the dialog title bar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    /**
     * Write the track id and artist name to the savestate bundle
     * @param outState Bundle
     */
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // save the previously used track id
        outState.putInt(ArtistProvider.Track.COL_ID, mSelectedTrackId);

        // save the previously used artist name
        outState.putString(ArtistProvider.Artist.COL_NAME, mArtistName);
    }

    /**
     * Initialize the loader once the activity is created
     * @param savedInstanceState Bundle
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        getLoaderManager().initLoader(TRACK_LOADER, null, this);

        Log.i(LOG_TAG, "============> initLoader to reload track details for track id: " + mSelectedTrackId);

        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Set the dimentsions of the dialog player fragment
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // try to get the dialog by tag name
        Fragment fragment = getFragmentManager().findFragmentByTag(PLAYERDIALOGFRAGMENT_TAG);

        // only resize the fragment when it's a dialog
        if (fragment != null) {
            int width = getResources().getDimensionPixelSize(R.dimen.player_dialog_width);
            int height = getResources().getDimensionPixelSize(R.dimen.player_dialog_height);
            getDialog().getWindow().setLayout(width, height);
        }
    }

    /**
     * Connection to the PlayerService
     */
    private ServiceConnection mPlayerConnection = new ServiceConnection()
    {
        // use classname when logging
        private final String LOG_TAG = ServiceConnection.class.getSimpleName();

        /**
         * When connecting to the service pass the track and set it to bound
         * @param name ComponentName
         * @param service IBinder
         */
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            Log.i(LOG_TAG, "============> onServiceConnected called");

            // get the playerservice binder
            PlayerBinder binder = (PlayerBinder) service;

            // get playerservice
            mPlayerService = binder.getService();

            // set the playerservice to bound state
            mPlayerServiceBound = true;

            // check if a new artist or track was selected
            if (mSelectedTrackId != mPlayerService.getSelectedTrackId()) {
                mPlayerService.stop();
                mPlayerService.selectTrack(mSelectedTrackId);
                mPlayerService.setTopTracks(mTopTracks);
            }

            // update extisting player state, or start playing immediately
            if (mPlayerService.isPlaying()) {
                // playing (on rotation)
                updateTrackDuration(mPlayerService.getDuration());
                togglePlayButton(true);
            } else if (mPlayerService.isPaused()) {
                // paused (on rotation)
                updateTrackDuration(mPlayerService.getDuration());
                updateTrackPosition(mPlayerService.getPosition());
                togglePlayButton(false);
            } else if (!mPlayerService.isCompleted()) {
                // not completed
                onPressPlay();
            }

            // make a messenger connection with the playerservice so we can also receive messages
            mPlayerService.setMessenger(new Messenger(mMessageHandler));
        }

        /**
         * On disconnecting from the service set it to unbound
         * @param name ComponentName
         */
        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            Log.i(LOG_TAG, "============> onServiceDisconnected called");

            // set the player service to unbound state
            mPlayerServiceBound = false;
        }
    };

    /**
     * Receive incoming messages from an external service using a weakreference,
     *  so we dont cause memory leaks
     */
    private static class IncomingHandler extends Handler
    {
        private final WeakReference<PlayerFragment> mPlayerFragment;

        IncomingHandler(PlayerFragment fragment)
        {
            mPlayerFragment = new WeakReference<PlayerFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg)
        {
            PlayerFragment fragment = mPlayerFragment.get();
            if (fragment != null) {
                fragment.handleMessage(msg);
            }
        }
    }

    /**
     * Handle messages received from the playerservice using the incominghandler
     * @param msg Message
     */
    public void handleMessage(Message msg)
    {
        int trackId = 0;
        int trackDuration = 0;
        int trackPosition = 0;

        switch (msg.what) {

            // handle 'track completed playing' message
            case MSG_TYPE_ON_COMPLETION:
                trackId = msg.getData().getInt("trackId");
                if ((trackId != 0) && (trackId == mSelectedTrackId)) {
                    Log.i(LOG_TAG, "============> Handling message: MSG_TYPE_ON_COMPLETION - trackId: "+ trackId);
                    togglePlayButton(false);
                    updateTrackPosition(0);
                }
                break;

            // handle 'track ready and playing' message
            case MSG_TYPE_ON_PREPARED:
                trackId = msg.getData().getInt("trackId");
                trackDuration = msg.getData().getInt("trackDuration");
                if ((trackId != 0) && (trackId == mSelectedTrackId)) {
                    Log.i(LOG_TAG, "============> Handling message: MSG_TYPE_ON_PREPARED - trackDuration: "+ trackDuration);
                    updateTrackDuration(trackDuration);
                }
                break;

            // handle 'updated current position' message
            case MSG_TYPE_CURRENT_POSITION:
                trackId = msg.getData().getInt("trackId");
                trackPosition = msg.getData().getInt("trackPosition");
                if ((trackId != 0) && (trackId == mSelectedTrackId)) {
                    Log.i(LOG_TAG, "============> Handling message: MSG_TYPE_CURRENT_POSITION - trackPosition: "+ trackPosition);
                    updateTrackPosition(trackPosition);
                }
                break;

            default:
                Log.i(LOG_TAG, "============> Received unknown message type");
        }
    }

    /**
     * Start playing the currently selected track
     */
    public void onPressPlay()
    {
        // check if the playerserice is bound
        if (mPlayerServiceBound) {

            // check network available
            if (Utility.isNetworkAvailable(getActivity())) {

                if (mPlayerService.isPlaying()) {
                    // if a track is already playing

                    // pause the currently playing track
                    mPlayerService.pause();
                    togglePlayButton(false);

                } else if (mPlayerService.isPaused()) {
                    // if a track is paused

                    // resume playing the currently pasued track
                    mPlayerService.resume();
                    togglePlayButton(true);

                } else {

                    // else, select the currently selected track in the playerservice and start playing
                    mPlayerService.selectTrack(mSelectedTrackId);
                    mPlayerService.play();
                    togglePlayButton(true);
                }
            } else {
                mToast = Utility.showToast(getActivity(), mToast, getString(R.string.network_required_notice));
            }
        }
    }

    /**
     * Select/play the previous track in the toptracks list
     */
    public void onPressPrevious()
    {
        updateTrackDuration(0);
        updateTrackPosition(0);

        // check if the playerserice is bound
        if (mPlayerServiceBound) {

            // check network available
            if (Utility.isNetworkAvailable(getActivity())) {

                // toggle the play button
                if (mPlayerService.isPlaying() || mPlayerService.isPaused()) {
                    togglePlayButton(true);
                }

                // select the previous track and get the selected track id returned from the playerservice
                mSelectedTrackId = mPlayerService.playPrevious();
            } else {
                mToast = Utility.showToast(getActivity(), mToast, getString(R.string.network_required_notice));
            }
        }

        // update the view to the previous track
        populateView(getSelectedTrack(mSelectedTrackId));
    }

    /**
     * Select/play the next track in the toptracks list
     */
    public void onPressNext()
    {
        updateTrackDuration(0);
        updateTrackPosition(0);

        // check if the playerserice is bound
        if (mPlayerServiceBound) {

            // check network available
            if (Utility.isNetworkAvailable(getActivity())) {

                // toggle the play button
                if (mPlayerService.isPlaying() || mPlayerService.isPaused()) {
                    togglePlayButton(true);
                }

                // select the next track and get the selected track id returned from the playerservice
                mSelectedTrackId = mPlayerService.playNext();
            } else {
                mToast = Utility.showToast(getActivity(), mToast, getString(R.string.network_required_notice));
            }
        }

        // update the view to the previous track
        populateView(getSelectedTrack(mSelectedTrackId));
    }

    /**
     * Toggle the play/pause state of the play button imageview
     * @param playing boolean
     */
    public void togglePlayButton(boolean playing)
    {
        View view = getView();
        if (view != null) {
            ImageView playImage = (ImageView) view.findViewById(R.id.player_play_imageview);
            if (playing) {
                playImage.setImageResource(android.R.drawable.ic_media_pause);
            } else {
                playImage.setImageResource(android.R.drawable.ic_media_play);
            }
        }
    }

    /**
     * Set the value of the track duration textview right below the seekbar
     * @param duration int
     */
    private void updateTrackDuration(int duration)
    {
        if (duration >= 0) {
            int secDuration = duration/1000;
            updateTimeTextView(mTrackDuration, secDuration);
            mPlayerSeekbar.setMax(secDuration);
        } else {
            updateTimeTextView(mTrackDuration, 0);
            mPlayerSeekbar.setMax(0);
        }
    }

    /**
     * Set the value of the seekbar and track position textview left below the seekbar
     * @param position int
     */
    private void updateTrackPosition(int position)
    {
        if (position >= 0) {
            int secPosition = position/1000;
            updateTimeTextView(mTrackPosition, secPosition);
            mPlayerSeekbar.setProgress(secPosition);
        } else {
            updateTimeTextView(mTrackPosition, 0);
            mPlayerSeekbar.setProgress(0);
        }
    }

    /**
     * Update the value of the time textviews to a format of minutes:seconds
     * @param view TextView
     * @param time int
     */
    private void updateTimeTextView(TextView view, int time)
    {
        int minutes = 0;
        int seconds = 0;

        // check if more than 60 seconds
        if (time >= 60) {
            minutes = time/60;
            seconds = time - (minutes*60);
        } else {
            seconds = time;
        }

        // add minutes to string
        String strTime = minutes +":";

        // if seconds smaller than 10 add zero to string
        if (seconds < 10) {
            strTime += "0";
        }

        // add seconds to string
        strTime += String.valueOf(seconds);

        // set text value
        view.setText(strTime);
    }

    /**
     * Load the track details from the database for given track id when the loader is called
     * @param i int
     * @param bundle Bundle
     * @return Cursor containing the tracks
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle)
    {
        if (mSelectedTrackId != 0) {

            Log.i(LOG_TAG, "============> Loading track from database with trackId: " + mSelectedTrackId);

            // load details of selected track
            TrackItem selectedTrack = getTrack(mSelectedTrackId);

            if (selectedTrack != null) {

                // return details of top tracks of selected artist
                return new CursorLoader(getActivity(),
                        ArtistProvider.uriTracks,
                        null,
                        ArtistProvider.Track.COL_ARTIST_ID + " = ?",
                        new String[]{String.valueOf(selectedTrack.getArtistId())},
                        null);

            } else {
                // selected track not found
                return null;
            }
        } else {
            // selected track id = 0
            return null;
        }
    }

    /**
     * When finished loading from the database, update view with data from the received cursor
     * @param cursorLoader Loader
     * @param cursor Cursor
     */
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor)
    {
        Log.i(LOG_TAG, "============> onLoadFinished");

        if (cursor != null) {

            // check if we have at least one track item
            if (cursor.getCount() > 0) {

                // initialize toptracks list
                mTopTracks = new ArrayList<TrackItem>();

                // initialize selected trackitem to populate the view
                TrackItem selectedTrack = null;

                // get all tracks from the database cursor and populate the toptracks list
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    TrackItem track = new TrackItem(cursor);
                    mTopTracks.add(track);

                    // get the selected trackitem
                    if (mSelectedTrackId == track.getId()) {
                        selectedTrack = track;
                    }

                    cursor.moveToNext();
                }

                // populate the view with selected trackitem details
                if (selectedTrack != null) {
                    populateView(selectedTrack);

                    // create the playservice intent if it was not created yet,
                    //  do this here instead of in oncreate because we want to start playing the
                    //  selected track immediately and therefor it is necceserry to have the track-
                    //  details loaded. If we rely on onstart it will/may break on device rotate
                    if (mPlayIntent == null) {

                        // create an intent that we can use to start the playerservice
                        mPlayIntent = new Intent(getActivity(), PlayerService.class);

                        // bind the playerservice by passing the playintent through the playerconnection, and start it
                        getActivity().bindService(mPlayIntent, mPlayerConnection, Context.BIND_AUTO_CREATE);
                        getActivity().startService(mPlayIntent);

                        // create the incominghandler so we can start receiving messages from the playerservice
                        mMessageHandler = new IncomingHandler(this);

                        Log.i(LOG_TAG, "============> PlayerService started");
                    }
                }
            }
        }
    }

    /**
     * Empty the cursoradapter by setting null instead of a cursor
     * @param cursorLoader Loader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {}

    /**
     * Set the image and text values of selected track in the fragment_player view
     * @param track TrackItem
     */
    private void populateView(TrackItem track)
    {
        // get the view for this detail fragment
        View view = getView();

        if (view != null) {

            // artist name
            if (mArtistName != null) {
                TextView artistNameView = (TextView) view.findViewById(R.id.artist_name_textview);
                artistNameView.setText(mArtistName);
            }

            // album name
            String albumName = track.getAlbumName();
            TextView albumNameView = (TextView) view.findViewById(R.id.album_name_textview);
            albumNameView.setText(albumName);

            // album image
            String albumImageUrl = track.getAlbumImageUrl();
            ImageView albumImageView = (ImageView) view.findViewById(R.id.album_imageview);
            if ((albumImageUrl != null) && (albumImageUrl.length() != 0)) {
                // use picasso to load the image
                Picasso.with(getActivity())
                        .load(albumImageUrl)
                        .into(albumImageView);
            } else {

                // else use the image not available icon
                albumImageView.setImageResource(R.mipmap.ic_launcher);
            }

            // track name
            String name = track.getName();
            TextView nameView = (TextView) view.findViewById(R.id.name_textview);
            nameView.setText(name);
        }
    }

    /**
     * Select track from toptracks arraylist
     * @param selectedTrackId int
     * @return TrackItem
     */
    private TrackItem getSelectedTrack(int selectedTrackId)
    {
        TrackItem selectedTrack = null;

        if ((mTopTracks != null) && (mTopTracks.size() > 0)) {
            for (TrackItem track : mTopTracks) {
                if (track.getId() == selectedTrackId) {
                    selectedTrack = track;
                }
            }
        }

        return selectedTrack;
    }

    /**
     * Get the track details from the ContentResolver using ArtistProvider
     * @param trackId int
     * @return TrackItem
     */
    private TrackItem getTrack(int trackId)
    {
        TrackItem trackItem = null;

        // get the track details from the database
        Cursor trackCursor = getActivity().getContentResolver().query(
                ArtistProvider.uriTracks,
                null,
                ArtistProvider.Track.COL_ID +" = ?",
                new String[] { String.valueOf(trackId) },
                null
        );

        if (trackCursor != null) {
            if (trackCursor.getCount() > 0) {
                trackCursor.moveToFirst();

                // create trackitem object from cursor
                trackItem = new TrackItem(trackCursor);
            }
            trackCursor.close();
        }

        return trackItem;
    }

    /**
     * Release the playerservice when we exit the playeractivity
     */
    @Override
    public void onDestroy()
    {
        Log.i(LOG_TAG, "============> onDestroy called");

        // remove messenger reference from playerservice
        if(mPlayerService != null) {
            mPlayerService.removeMessenger();
        }

        // unbind the playerconnection
        if (mPlayerConnection != null) {
            getActivity().unbindService(mPlayerConnection);
        }

        // indicate that the playerservice is not bound anymore
        mPlayerServiceBound = false;

        super.onDestroy();
    }
}