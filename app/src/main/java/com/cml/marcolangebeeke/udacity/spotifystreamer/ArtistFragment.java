package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistItem;
import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;

/**
 * Artist fragment class
 */
public class ArtistFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    // use classname when logging
    private final String LOG_TAG = ArtistFragment.class.getSimpleName();

    // constants for fragment tags
    private static final String PLAYERFRAGMENT_TAG = "PFTAG";
    private static final String PLAYERDIALOGFRAGMENT_TAG = "PDFTAG";

    // unique reference for the tracks cursor loader
    private static final int TRACKS_LOADER = 1;

    // timeout for reloading the tracks from the spotify api
    private static final int CACHE_TIMEOUT = 3600;

    // a common toast object for giving feedback
    protected Toast mToast;

    // the with the intent given artist id
    private int mArtistId = 0;

    private String mArtistName;

    // Track list adapter for displaying the top 10 tracks of an artist in the spotify api
    protected TrackListAdapter mTrackListAdapter;

    /**
     * On create of the artist fragment set the fragment_artist layout as contentview
     *
     * @param inflater LayoutInflator
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.i(LOG_TAG, "============> onCreateView called");

        // create a list adapter from the trackitems arraylist
        mTrackListAdapter = new TrackListAdapter(getActivity(), null, 0);

        // get the artist fragment layout as defined in the fragment_artist xml
        final View artistView = inflater.inflate(R.layout.fragment_artist, container, false);

        // find the listview and set the listadapter to it
        ListView trackListView = (ListView) artistView.findViewById(R.id.listview_track);
        trackListView.setAdapter(mTrackListAdapter);

        // on click on a track list item start the player activity view and pass the track id as intent
        trackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // check network available
                if (Utility.isNetworkAvailable(getActivity())) {

                    // get cursor from the selected item of the cursoradapter connected to the listview
                    Cursor trackCursor = (Cursor) parent.getItemAtPosition(position);
                    if ((trackCursor != null) && (trackCursor.getCount() >= position)) {

                        // get track id from selected cursor
                        int trackId = trackCursor.getInt(trackCursor.getColumnIndex(ArtistProvider.Track.COL_ID));

                        // create a bundle for sending the track id and artist name as arguments to the playerfragment
                        Bundle args = new Bundle();
                        args.putInt(ArtistProvider.Track.COL_ID, trackId);
                        args.putString(ArtistProvider.Artist.COL_NAME, mArtistName);

                        // create the playerfragment
                        PlayerFragment playerFragment = new PlayerFragment();
                        playerFragment.setArguments(args);

                        // based on the activity we are in:
                        if (getActivity().getClass() == ArtistActivity.class) {

                            // open the playerfragment fullscreen on a phone
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(android.R.id.content, playerFragment, PLAYERFRAGMENT_TAG);
                            transaction.addToBackStack(null);
                            transaction.commit();

                        } else if (getActivity().getClass() == MainActivity.class) {

                            // or, open the playerfragment as a dialog on a tablet
                            playerFragment.show(getActivity().getSupportFragmentManager(), PLAYERDIALOGFRAGMENT_TAG);
                        }
                    }
                } else {
                    mToast = Utility.showToast(getActivity(), mToast, getString(R.string.network_required_notice));
                }
            }
        });

        // get given arguments (which we retrieve in tablet mode)
        Bundle args = getArguments();

        // restore a previous state if present
        if (savedInstanceState != null) {

            // check if artist id was stored
            if (savedInstanceState.containsKey(ArtistProvider.Artist.COL_ID)) {
                mArtistId = savedInstanceState.getInt(ArtistProvider.Artist.COL_ID, 0);
            }

            // check if artist name was stored
            if (savedInstanceState.containsKey(ArtistProvider.Artist.COL_NAME)) {
                mArtistName = savedInstanceState.getString(ArtistProvider.Artist.COL_NAME);
            }

        } else if (args != null) {

            // get the artist id from the fragment arguments
            mArtistId = args.getInt(ArtistProvider.Artist.COL_ID, 0);

            // get the artist name from the fragment arguments
            mArtistName = args.getString(ArtistProvider.Artist.COL_NAME);

        } else {

            // if no previous state available get the intent given when this activity was launched
            Intent intent = getActivity().getIntent();

            // get the artist id given with the intent
            if ((intent != null) && (intent.hasExtra(ArtistProvider.Artist.COL_ID))) {
                mArtistId = intent.getIntExtra(ArtistProvider.Artist.COL_ID, 0);
            }

            // get the artist name given with the intent
            if ((intent != null) && (intent.hasExtra(ArtistProvider.Artist.COL_NAME))) {
                mArtistName = intent.getStringExtra(ArtistProvider.Artist.COL_NAME);
            }
        }

        // get the toptracks of given artist from the spotify api
        if (mArtistId != 0) {
            getTracks(mArtistId);
        }

        return artistView;
    }

    /**
     * Save the the current state before leaving the activity
     *
     * @param outState Bundle
     */
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // save the previously used artist id
        outState.putInt(ArtistProvider.Artist.COL_ID, mArtistId);

        // save the previously used artist name
        outState.putString(ArtistProvider.Artist.COL_NAME, mArtistName);
    }

    /**
     * Initialize the loader once the activity is created
     *
     * @param savedInstanceState Bundle
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        getLoaderManager().initLoader(TRACKS_LOADER, null, this);

        Log.d(LOG_TAG, "============> initLoader to reload tracks for artist id: " + mArtistId);

        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Call the async get artist toptracks task
     *
     * @param artistId int
     */
    private void getTracks(int artistId)
    {
        int timestamp = (int) (System.currentTimeMillis()/1000);

        // find tracks for given artist that are newer than cache timeout
        Cursor tracksCursor = getActivity().getContentResolver().query(
                ArtistProvider.uriTracks,
                null,
                ArtistProvider.Track.COL_ARTIST_ID +" = ? AND "+ ArtistProvider.Track.COL_MODIFIED +" > ? ",
                new String[] { String.valueOf(artistId), String.valueOf(timestamp - CACHE_TIMEOUT) },
                null
        );

        // if no tracks found, it means that tracks are older or do not exist at all
        if ((tracksCursor == null) || (tracksCursor.getCount() == 0)) {

            Log.d(LOG_TAG, "============> Calling spotify API to load tracks for artistId: " + artistId);

            // check network available
            if (Utility.isNetworkAvailable(getActivity())) {

                // get artist details from database
                ArtistItem artist = getArtist(artistId);

                // create and execute a new get artist toptracks task
                GetTracksTask topTracksTask = new GetTracksTask(this, TRACKS_LOADER);
                topTracksTask.execute(artist);
            }
        }

        if (tracksCursor != null) {
            tracksCursor.close();
        }
    }

    /**
     * Get the artist details from the database for given artist id and return a artistitem object
     *
     * @param artistId int
     * @return ArtistItem
     */
    public ArtistItem getArtist(int artistId)
    {
        ArtistItem artistItem = null;

        // get the query details from the database
        Cursor artistCursor = getActivity().getContentResolver().query(
                ArtistProvider.uriArtists,
                null,
                ArtistProvider.Artist.COL_ID +" = ?",
                new String[] { String.valueOf(artistId) },
                null
        );

        if (artistCursor != null) {
            if (artistCursor.getCount() > 0) {
                artistCursor.moveToFirst();

                // create queryitem object from cursor
                artistItem = new ArtistItem(artistCursor);
            }
            artistCursor.close();
        }

        return artistItem;
    }

    /**
     * Load the tracks from the database for given artist when the loader is called
     *
     * @param i int
     * @param bundle Bundle
     * @return Cursor containing the tracks
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle)
    {
        if (mArtistId != 0) {

            Log.d(LOG_TAG, "============> Loading tracks from database with artistId: " + mArtistId);

            return new CursorLoader(getActivity(),
                    ArtistProvider.uriTracks,
                    null,
                    ArtistProvider.Track.COL_ARTIST_ID +" = ?",
                    new String[]{ String.valueOf(mArtistId) },
                    ArtistProvider.Track.COL_POSITION +" ASC");
        } else {
            return null;
        }
    }

    /**
     * When finished loading from the database, set the loaded cursor to the cursoradapter to update the listview
     *
     * @param cursorLoader Loader
     * @param cursor Cursor
     */
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor)
    {
        mTrackListAdapter.swapCursor(cursor);
    }

    /**
     * Empty the cursoradapter by setting null instead of a cursor
     *
     * @param cursorLoader Loader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader)
    {
        mTrackListAdapter.swapCursor(null);
    }
}
