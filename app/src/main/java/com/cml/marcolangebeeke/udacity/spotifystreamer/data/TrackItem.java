package com.cml.marcolangebeeke.udacity.spotifystreamer.data;

import android.database.Cursor;

/**
 * Track item class containing the details of a track
 */
public class TrackItem
{
    // track id
    private int mId;

    // spotify id of the track
    private String mSpotifyTrackId;

    // artist id
    private int mArtistId;

    // spotify id of the artist
    private String mSpotifyArtistId;

    // name of the track
    private String mName;

    // name of the album
    private String mAlbumName;

    // thumbnail url of the album
    private String mAlbumThumbnailUrl;

    // image url of the album
    private String mAlbumImageUrl;

    // media url of the preview
    private String mPreviewUrl;

    /**
     * Constructor
     *
     * @param spotifyTrackId String
     * @param spotifyArtistId String
     * @param name String
     * @param albumName String
     * @param albumThumbnailUrl String
     * @param albumImageUrl String
     * @param previewUrl String
     */
    public TrackItem(String spotifyTrackId, int artistId, String spotifyArtistId, String name, String albumName, String albumThumbnailUrl, String albumImageUrl, String previewUrl)
    {
        mSpotifyTrackId = spotifyTrackId;
        mArtistId = artistId;
        mSpotifyArtistId = spotifyArtistId;
        mName = name;
        mAlbumName = albumName;
        mAlbumThumbnailUrl = albumThumbnailUrl;
        mAlbumImageUrl = albumImageUrl;
        mPreviewUrl = previewUrl;
    }

    public TrackItem(Cursor cursor)
    {
        setId(cursor.getInt(cursor.getColumnIndex(ArtistProvider.Track.COL_ID)));
        setSpotifyTrackId(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_SPOTIFY_TRACK_ID)));
        setArtistId(cursor.getInt(cursor.getColumnIndex(ArtistProvider.Track.COL_ARTIST_ID)));
        setSpotifyArtistId(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_SPOTIFY_ARTIST_ID)));
        setName(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_NAME)));
        setAlbumName(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_ALBUM_NAME)));
        setAlbumThumbnailUrl(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_THUMBNAIL_URL)));
        setAlbumImageUrl(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_IMAGE_URL)));
        setPreviewUrl(cursor.getString(cursor.getColumnIndex(ArtistProvider.Track.COL_PREVIEW_URL)));
    }

    /**
     * Get the id of the track
     * @return int
     */
    public int getId()
    {
        return mId;
    }

    /**
     * Set the id of the track
     * @param id int
     */
    public void setId(int id)
    {
        mId = id;
    }

    /**
     * Get the spotify id of the track
     * @return String
     */
    public String getSpotifyTrackId()
    {
        return mSpotifyTrackId;
    }

    /**
     * Set the spotify id of the track
     * @param trackId String
     */
    public void setSpotifyTrackId(String trackId)
    {
        mSpotifyTrackId = trackId;
    }

    /**
     * Get the id of the artist
     * @return int
     */
    public int getArtistId()
    {
        return mArtistId;
    }

    /**
     * Set the id of the artist
     * @param artistId int
     */
    public void setArtistId(int artistId)
    {
        mArtistId = artistId;
    }

    /**
     * Get the spotify id of the artist
     * @return String
     */
    public String getSpotifyArtistId()
    {
        return mSpotifyArtistId;
    }

    /**
     * Set the spotify id of the artist
     * @param artistId String
     */
    public void setSpotifyArtistId(String artistId)
    {
        mSpotifyArtistId = artistId;
    }

    /**
     * Get the name of the track
     * @return String
     */
    public String getName()
    {
        return mName;
    }

    /**
     * Set the name of the track
     * @param name String
     */
    public void setName(String name)
    {
        mName = name;
    }

    /**
     * Get the name of the album
     * @return String
     */
    public String getAlbumName()
    {
        return mAlbumName;
    }

    /**
     * Set the name of the album
     * @param albumName String
     */
    public void setAlbumName(String albumName)
    {
        mAlbumName = albumName;
    }

    /**
     * Get the url of the album thumbnail
     * @return String
     */
    public String getAlbumThumbnailUrl()
    {
        return mAlbumThumbnailUrl;
    }

    /**
     * Set the url of the album thumbnail
     * @param albumThumbnailUrl String
     */
    public void setAlbumThumbnailUrl(String albumThumbnailUrl)
    {
        mAlbumThumbnailUrl = albumThumbnailUrl;
    }

    /**
     * Get the url of the album image
     * @return String
     */
    public String getAlbumImageUrl()
    {
        return mAlbumImageUrl;
    }

    /**
     * Set the url of the album image
     * @param albumImageUrl String
     */
    public void setAlbumImageUrl(String albumImageUrl)
    {
        mAlbumImageUrl = albumImageUrl;
    }

    /**
     * Get the url of the preview stream
     * @return String
     */
    public String getPreviewUrl()
    {
        return mPreviewUrl;
    }

    /**
     * Set the url of the preview stream
     * @param previewUrl String
     */
    public void setPreviewUrl(String previewUrl)
    {
        mPreviewUrl = previewUrl;
    }
}