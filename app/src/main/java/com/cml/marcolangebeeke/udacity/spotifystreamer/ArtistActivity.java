package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Artist Activity class
 */
public class ArtistActivity extends AppCompatActivity
{
    // use classname when logging
    private final String LOG_TAG = ArtistActivity.class.getSimpleName();

    /**
     * On create of the artist activity set the activity_artist layout as contentview
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_artist);
    }

    @Override
    protected void onDestroy()
    {
        Log.i(LOG_TAG, "============> onDestroy called");

        super.onDestroy();
    }
}
