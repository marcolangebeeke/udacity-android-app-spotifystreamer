package com.cml.marcolangebeeke.udacity.spotifystreamer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.cml.marcolangebeeke.udacity.spotifystreamer.data.ArtistProvider;
import com.cml.marcolangebeeke.udacity.spotifystreamer.player.PlayerService;

/**
 * Main Activity class
 */
public class MainActivity extends AppCompatActivity implements MainFragment.Callback
{
    // use classname when logging
    private final String LOG_TAG = MainActivity.class.getSimpleName();

    // indicator to know if we are on a 'two-pane' device (tablet)
    private boolean mTwoPane;

    // constant for artistfragment tag
    private static final String ARTISTFRAGMENT_TAG = "AFTAG";

    /**
     * On create of the main activity set the activity_main layout as contentview
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // set activity_main layout as content view
        setContentView(R.layout.activity_main);

        // test if we are in table or phone mode (the artist_container view is only defined in the sw600dp layout)
        if (findViewById(R.id.artist_container) != null) {
            mTwoPane = true;

            // replace the artist container with an empty artistfragment
            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.artist_container, new ArtistFragment(), ARTISTFRAGMENT_TAG)
                        .commit();
            }
        } else {
            mTwoPane = false;
        }
    }

    /**
     * Are we on a 'two-pane' device?
     * @return boolean
     */
    public boolean isTwoPaneDevice()
    {
        return mTwoPane;
    }

    /**
     * Depending on the table/phone mode open the artistfragment or activity
     * @param artistId int
     * @param artistName String
     */
    public void onArtistSelected(int artistId, String artistName)
    {
        if (mTwoPane) {

            // tablet mode - add artist id and name as arguments to the artistfragment
            Bundle args = new Bundle();
            args.putInt(ArtistProvider.Artist.COL_ID, artistId);
            args.putString(ArtistProvider.Artist.COL_NAME, artistName);

            ArtistFragment fragment = new ArtistFragment();
            fragment.setArguments(args);

            // replace the artist container with the artistfragment with given arguments
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.artist_container, fragment, ARTISTFRAGMENT_TAG)
                    .commit();

        } else {

            // phone mode - open the artistactivity and pass the artist id and name using an intent
            Intent intent = new Intent(this, ArtistActivity.class);
            intent.putExtra(ArtistProvider.Artist.COL_ID, artistId);
            intent.putExtra(ArtistProvider.Artist.COL_NAME, artistName);
            startActivity(intent);
        }
    }

    /**
     * If we 'unselect' an artist (run a new artists search) we clear the artistfragment containing the toptracks
     */
    public void onArtistUnSelected()
    {
        if (mTwoPane) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.artist_container, new ArtistFragment(), ARTISTFRAGMENT_TAG)
                    .commit();
        }
    }

    /**
     * On destroy stop the playerservice, if it is not on-rotation and really an app destroy
     */
    @Override
    protected void onDestroy()
    {
        Log.i(LOG_TAG, "============> onDestroy called");

        if (!isChangingConfigurations()) {
            Log.v(LOG_TAG, "============> App is being closed so lets stop the playerservice");

            // get reference to playerservice and stop it
            Intent mPlayIntent = new Intent(this, PlayerService.class);
            stopService(mPlayIntent);
        }

        super.onDestroy();
    }
}
